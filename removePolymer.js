var poly = "disable_polymer=1";
// if the URL does not contain poly
if (window.location.href.indexOf(poly) == -1) { 
	// If the URL already contains a ?
	if (window.location.href.indexOf("?") !== -1) {
		window.location.href += "&" + poly;
	} else {
		window.location.href += "?" + poly;
	}
}

for(var i = 0; i < document.links.length; i++) {
	if(document.links[i].href.indexOf("youtube.com") !== -1) {
		// if the URL does not contain poly
		if (document.links[i].href.indexOf(poly) == -1) { 
			// If the URL already contains a ?
			if (document.links[i].href.indexOf("?") !== -1) {
				document.links[i].href += "&" + poly;
			} else {
				document.links[i].href += "?" + poly;
			}
		}
	}
}