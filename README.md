# Youtube NoPolymer for Firefox

A quick Firefox Extension to disable Polymer on Youtube 

According to [@cpeterso](https://twitter.com/cpeterso/status/1021626510296285185), `YouTube page load is 5x slower in Firefox and Edge than in Chrome because YouTube's Polymer redesign relies on the deprecated Shadow DOM v0 API only implemented in Chrome`. This Firefox Addon restores the older view and disables Polymer, making YouTube load about 5x faster.

### Install Instructions

There is currently a private xpi for this extension. Please get in contact with me through Discord at `MasterChief_John-117#1911`. This will ensure I can provide the best support and latest version without publishing it to the Mozilla library.